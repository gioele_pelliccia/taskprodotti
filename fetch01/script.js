function CountCategorie(){
    $.ajax({
        
        url: "categorie.json",
        method: "GET",
        
        success: function (response){
            let counter;
            let array = []
            for(let item in response){
                if(!array.includes(item.categoria)){
                    array.push(item.categoria)
                    counter++
                }
            }
            $("#numCategorie").val(`Numero Categorie : ${counter}`);
        },error: function(errore){
            console.log(errore)
            alert("Errore")
        }
    });
}    

function stampaAbb(){
    $.ajax({
        
        url: "categorie.json",
        method: "GET",
        
        success: function (response) {
            let content="";
            
            for(let item of response){
                if(item.categoria=="abbigliamento"){
                    content+=`
                            <tr>
                                <td>${item.prodotto}</td>
                                <td>${item.genere}</td>
                                <td>${item.prezzo}</td>
                                <td>${item.venduti}</td>
                            </tr>    
                            `
                }            
            }
            $("#abbContent").html(content);
            
        },error: function(errore){
            console.log(errore)
            alert("Errore")
        }
    });
}
function stampaCalz(){
    $.ajax({
        
        url: "categorie.json",
        method: "GET",
        
        success: function (response) {
            let content="";
            
            for(let item of response){
                if(item.categoria=="calzature"){
                    content+=`
                            <tr>
                                <td>${item.prodotto}</td>
                                <td>${item.genere}</td>
                                <td>${item.prezzo}</td>
                                <td>${item.venduti}</td>
                            </tr>    
                            `
                }            
            }
            $("#calzContent").html(content);
            
        },error: function(errore){
            console.log(errore)
            alert("Errore")
        }
    });
}
function stampaBorse(){
    $.ajax({
        
        url: "categorie.json",
        method: "GET",
        
        success: function (response) {
            let content="";
            
            for(let item of response){
                if(item.categoria=="borse"){
                    content+=`
                            <tr>
                                <td>${item.prodotto}</td>
                                <td>${item.genere}</td>
                                <td>${item.prezzo}</td>
                                <td>${item.venduti}</td>
                            </tr>    
                            `
                }            
            }
            $("#borseContent").html(content);
            
        },error: function(errore){
            console.log(errore)
            alert("Errore")
        }
    });
}
function stampaUomo(){
    $.ajax({
        
        url: "categorie.json",
        method: "GET",
        
        success: function (response) {
            let content="";
            
            for(let item of response){
                if(item.genere=="uomo"){
                    content+=`
                            <tr>
                                <td>${item.prodotto}</td>
                                <td>${item.categoria}</td>
                                <td>${item.prezzo}</td>
                                <td>${item.venduti}</td>
                            </tr>    
                            `
                }            
            }
            $("#uomoContent").html(content);
            
        },error: function(errore){
            console.log(errore)
            
        }
    });
}
function stampaDonna(){
    $.ajax({
        
        url: "categorie.json",
        method: "GET",
        
        success: function (response) {
            let content="";
            
            for(let item of response){
                if(item.genere=="donna"){
                    content+=`
                            <tr>
                                <td>${item.prodotto}</td>
                                <td>${item.categoria}</td>
                                <td>${item.prezzo}</td>
                                <td>${item.venduti}</td>
                            </tr>    
                            `
                }            
            }
            $("#donnaContent").html(content);
            
        },error: function(errore){
            console.log(errore)
            
        }
    });
}
function stampaUnderTrenta(){
    $.ajax({
        
        url: "categorie.json",
        method: "GET",
        
        success: function (response) {
            let content="";
            
            for(let item of response){
                if(item.prezzo<=30){
                    content+=`
                            <tr>
                                <td>${item.prodotto}</td>
                                <td>${item.categoria}</td>
                                <td>${item.genere}</td>
                                <td>${item.prezzo}</td>
                                <td>${item.venduti}</td>
                            </tr>    
                            `
                }            
            }
            $("#30Content").html(content);
            
        },error: function(errore){
            console.log(errore)
            
        }
    });
}
function stampaTopFive(){
    $.ajax({
        
        url: "categorie.json",
        method: "GET",
        
        success: function (response) {
            let topVendite=Array.from(response).sort((a, b) => parseFloat(b.venduti) - parseFloat(a.venduti)).slice(0,5);

            let content="";
            
            for(let item of topVendite){
                
                    content+=`
                            <tr>
                                <td>${item.prodotto}</td>
                                <td>${item.categoria}</td>
                                <td>${item.genere}</td>
                                <td>${item.prezzo}</td>
                                <td>${item.venduti}</td>
                            </tr>    
                            `
                          
            }
            $("#top5Content").html(content);
            
        },error: function(errore){
            console.log(errore)
            alert("Errore")
        }
    });
}
// function showTables(){
//     $(".row").removeAttr("class","d-none");
// }
$(document).ready(function () {
    stampaAbb()
    stampaCalz()
    stampaBorse()
    stampaUomo()
    stampaDonna()
    stampaUnderTrenta()
    stampaTopFive()
    CountCategorie()
    $("#btnTabelle").click(function (e) { 
        showTables()

        
    });
    
});